NOR Page Allocation
==========================

NPA 機制是 ITE 針對車用儀表應用得頻繁儲存一些小尺寸的資料(譬如里程數)所提出的一個解決方案。相對於使用完整檔案系統來儲存資料需要在儲存一次資料的過程中至少寫入兩個以上的 block(甚至需要先 erase block，若該檔案系統支援日誌系統需要寫入更多 block)，NPA 機制只需寫入比資料大一點點的 bytes，並且只有在現有 block 寫滿了要寫入新 block 的時候才需要 erase block。整個所需的最大寫入時間可事先得知，並且在最後一筆資料寫壞時仍能復原到上一筆資料。

為了確保資料寫入的完整性(雖然有錯誤復原機制，但仍只能復原到上一筆正確寫入的資料)，在斷電後，將透過內建電池的電能來寫入最後一筆資料。須請板端同仁協助整理電路，包括加裝電池，以及銜接關機的 GPIO 通知。另外，根據 EVB 測試，電池最小容量的保險範圍，大約是斷電後，須維持 IC 正常運作 5 ms 以上，由於斷電 ISR 的資料寫入行為，主要是透過 timer pending function 去實作，所以也必須根據客戶的 task priority 規劃，來決定最終電池容量的大小，可以透過量波形來確認。

Kconfig
------------------

- :menuselection:`System --> Internal Settings --> Hardware encryption Enable`

  NPA 機制使用 CRC 確認待存資料的正確性，須勾選 DPU_ENABLE 來加速 CRC 的運算。

- :menuselection:`Display Control Board --> Allocate NOR Page`

  勾選此選項便能開啟 NPA 功能。

- :menuselection:`Display Control Board --> Allocate NOR Page --> Start address of NOR Page Allocation`

  NPA 所需的 NOR flash 儲存空間的起始位址，長度目前為兩個 64 KB block, 可根據應用需求來改變起始位置和長度。

- :menuselection:`Display Control Board --> Allocate NOR Page --> Shutdown GPIO Pin of NOR Page Allocation`

  此選項決定通知關機的 GPIO 編號，系統關機時該 GPIO 將由高電位被拉至低電位，ISR 資料寫入行為以此為依據。

NPA Flow with config.c
--------------------------------

目前整個 NPA 機制的操作流程已經整合在 config.c 這個程式碼檔案內。整個流程如下：

1) 操作流程
- 開機後，須先呼叫初始檢查 API (ConfigInit -> NORPageAllocationInitCheck)

- 需要寫入時，呼叫寫入 API, 參數為一個 uint32_t array, 預設格式為 total 4 byte, A 4 byte and B 4 byte (NPAConfigSave -> NORPageAllocationWrite)

- 需要讀取時，呼叫讀取 API, 取值參數為一個 uint32_t array, 預設格式為 total, A and B (npa_overhead_task -> NORPageAllocationRead, ConfigInit -> NORPageAllocationRead)

- 寫入讀取 API 支援 thread-safety

2) NPA 流程
- 循序循環由 start address 開始寫，每筆 16 byte(total, A, B and CRC data) 寫入 NOR

- 初始檢查 API, 先找最後一筆成功寫入的資料。

- 若找不到，檢查兩塊 NOR block 是否需要 erase (皆為 0xff 則不用 erase, 否則報錯並 erase)

- 若找到，檢查後續操作需要的 NOR block 是否已 erase, 若否則 erase

- 寫入 API, 由初始檢查的 index 開始寫。

- 寫該 block 最後一筆之前，檢查下一個 block 是否需要 erase, 若是則 erase

- 每筆寫入，都會從 NOR 讀回來比答案，若比錯則報錯，說明 NOR 已達使用壽命。

- 讀取 API 很單純，僅讀程式的 maintaining memory, 也做 CRC 校驗。

3) 測試流程
- 開機檢查。

- 讀取最後一筆資料。

- 將資料 item 都加 1 再寫入，睡 700 ms 跳回上一步。

- 過程中斷電測試。

4) 規格
- 每一次基本的 NOR Write 大約 200 至 300 us

- 每一次 NOR erase 最久可能達 2s (視環境), 過去經驗最長大致只測到 568 ms

- 時速 360 公里為上限，至少每秒需呼叫一次寫入，一次寫 0.1 公里

- 2 個 NOR block 共可存 8192 筆

- 壽命可存 819200000 次 => 819200000 / 10 = 81920000 KM

- NPA 比使用 file system 即時，並且節省 NOR 的空間及壽命。請仔細確認程式流程，因為這個方案會由 FAE 來客製化，不同 item 須規劃不同的儲存空間，甚至可能會開到多個 NOR block 來延長壽命。

