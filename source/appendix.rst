.. _appendix:

附錄
********************************


如何產生 DLM
----------------------------------------------------------------

本節說明如何將部分 library 加入到 DLM。

修改 :file:`<sdk_root>/opentros/boot/fa626/default.lds`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: c
  :emphasize-lines: 2,4-12


  #ifdef CFG_DLM_ENABLE
  #define DELAY_LOADED_MODULES  *liblwip.a: *libmhd4345x-LwIP_1_4_1.a: *libcurl.a:   *libbluetooth_bta.a
  #define EXCLUDE_DLM_STAT      EXCLUDE_FILE(DELAY_LOADED_MODULES)
  #define DLM_BSS               *libmhd4345x-LwIP_1_4_1.a:(.bss*)\
                                *liblwip.a:(.bss*)\
                                *libcurl.a:(.bss*)\
                                *libbluetooth_bta.a:(.bss*)\
                                *libbluetooth.a:(.bss*)\
                                *libmhd4345x-LwIP_1_4_1.a:(COMMON)\
                                *liblwip.a:(COMMON)\
                                *libcurl.a:(COMMON)\
                                *libbluetooth_bta.a:(COMMON)

  #else
  #define EXCLUDE_DLM_STAT
  #endif

- 在 DELAY_LOADED_MODULES 這個 define 可以添加要封裝到 DLM 的 library，然後在 DLM_BSS 做相對應的修改。
- 欲加到 DLM 的 library 必須使用 -no-lto 來編譯，否則無法順利加到 DLM 裡面，因此使用者要在此 library 對應的 CMakeLists.txt 加上以下這段程式碼：

  .. code-block:: cmake

    if (DEFINED CFG_DLM_ENABLE)
      add_definitions("-fno-lto")
    endif()


透過上述的修改後，程式在編譯後會自動將被歸類於 DLM 的 library 自動放入到 .dlm_xxx sections。

:file:`<sdk_root>/sdk/build.cmake`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

以下說明 ITE 針對 :file:`<sdk_root>/sdk/build.cmake` 所做的修改以產生 :file:`<project>.bin` 和 :file:`dlm.bin` 的過程。

- 從 :file:`<project>` 檔案(為elf格式) 移除以 .dlm_ 開頭的 sections 並產生 :file:`<project>.bin` 檔。

  .. code-block:: cmake

    add_custom_command(
        TARGET ${CMAKE_PROJECT_NAME}
        POST_BUILD
        COMMAND ${OBJCOPY}
        ARGS --remove-section=.dlm_* -O binary ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME} ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}.bin
    )

- 從 :file:`<project>` 檔案(為 elf 格式) 只取出以 .dlm_ 開頭的 sections 並產生 :file:`dlm.bin`。同樣從 bootloader 檔案只取出以 .dlm_ 開頭的 sections 並產生 :file:`bl_dlm.bin`。

  .. code-block:: cmake

    if (DEFINED CFG_DLM_ENABLE)
        if (NOT("${CMAKE_PROJECT_NAME}" STREQUAL "bootloader"))

            # make dlm
            add_custom_command(
                TARGET ${CMAKE_PROJECT_NAME}
                POST_BUILD
                COMMAND ${OBJCOPY}
                ARGS --only-section=.dlm_* -O binary ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME} ${CMAKE_CURRENT_BINARY_DIR}/dlm.bin
            )

        else()

            # make bootloader dlm
            if (DEFINED CFG_BL_DLM_ENABLE)
                add_custom_command(
                    TARGET ${CMAKE_PROJECT_NAME}
                    POST_BUILD
                    COMMAND ${OBJCOPY}
                    ARGS --only-section=.dlm_* -O binary ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME} ${CMAKE_CURRENT_BINARY_DIR}/bl_dlm.bin
                )
            endif()

        endif()
    endif()

- 使用 ``mkrom -z`` 進行 ucl compression，將 :file:`dlm.bin` 壓縮成 :file:`dlm.ucl`。


  .. code-block:: cmake

    if (DEFINED CFG_DLM_ENABLE)
        add_custom_command(
            TARGET ${CMAKE_PROJECT_NAME} POST_BUILD
            COMMAND mkrom
            ARGS -z -b 512K ${PROJECT_SOURCE_DIR}/sdk/target/ram/${CFG_RAM_INIT_SCRIPT} ${CMAKE_CURRENT_BINARY_DIR}/dlm.bin ${CMAKE_CURRENT_BINARY_DIR}/dlm.ucl
        )
    endif()

- 將 :file:`dlm.ucl` 指定放到 nor-unformatted-data3 區域。
- 使用 ``--nor-unformatted-data3-pos`` 指定 :file:`dlm.ucl` 要在 ROM 檔實際存放的位置。

  .. code-block:: cmake

    if (DEFINED CFG_DLM_ENABLE)
        set(args ${args} --nor-unformatted-data3 ${CMAKE_BINARY_DIR}/project/${CMAKE_PROJECT_NAME}/dlm.ucl)
        set(args ${args} --nor-unformatted-data3-pos ${CFG_UPGRADE_DLM_PACKAGE_POS})

        set(packargs ${packargs} --nor-unformatted-data3 ${CMAKE_BINARY_DIR}/project/${CMAKE_PROJECT_NAME}/dlm.ucl)
        set(packargs ${packargs} --nor-unformatted-data3-pos ${CFG_UPGRADE_DLM_PACKAGE_POS})
    endif()
