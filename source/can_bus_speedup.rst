.. _can_bus_speedup:

Canbus 快速喚醒
=====================================

ITE 採用在開機後短時間內立即啟動 can bus 模組以回應 can bus 上的訊號來解決 Canbus 快速喚醒的問題。因此客戶可在省電模式下將 ITE986x 晶片完全斷電，並且在晶片上電後晶片內的韌體可在 150 ms 內完全啟動 can bus 模組以接受並處理 can bus 上的訊號。ITE 發現開機過程中多數時間耗費在從 norflash 將程式載入到記憶體的過程中，為了加速這個過程，採取了一系列的最佳化步驟。底下是針對各個最佳化步驟的描述。

Kconfig 配置
----------------------------------------------------------------

首先，必須開啟 qconf 設定與建置工具裡的 :menuselection:`Upgrade --> Force skip Upgrade in boot time`，此功能可以在 bootloader 強制略過更新PKG，藉以縮短開機時間。

  .. image:: /_static/qconf_force_skip_upgrade.png

Bootloader
^^^^^^^^^^^^^^^^

Bootloader 放在 Rom #0x0 的位置。經實測將經過 UCL 壓縮後的 bootloader 從 norflash 解壓縮至記憶體所耗費的時間比單純將未壓縮的 bootloader 從 norflash 拷貝到記憶體所耗費的時間還久。為了達到快速開機，Bootloader 採用無壓縮的方式，因此必須關閉在 qconf 設定與建置工具裡的 :menuselection:`System --> Internal Settings --> Compress ROM` 選項。

  .. image:: /_static/qconf_compress_rom_disable.png

.. note::

  UCL壓縮是ITE自行開發的演算法，目前第一階段啟動程式只能採用UCL壓縮或不壓縮兩種模式。


.. raw:: latex

  \newpage

Image
^^^^^^^^^^^^^^^^

Image 放在 Rom 裡面的位置可以在 qconf 設定與建置工具裡的 :menuselection:`Upgrade --> Upgrade Image --> Image position` 選項設定。經實測將經過 Speedy 壓縮後的 code 從norflash 解壓縮至記憶體所耗費的時間比單純將未壓縮的 code 從 norflash 拷貝到記憶體所耗費的時間還短。為了達到快速開機且有效縮小 Image size 需要採用 Speedy 壓縮方式，請開啟qconf 設定與建置工具裡的 :menuselection:`System --> Internal Settings --> Speedy Decompress Enable` 選項。

  .. image:: /_static/qconf_image_position.png


  .. image:: /_static/qconf_speedy_decompress_enable.png

.. raw:: latex

  \newpage

DLM
^^^^^^^^^^^^^^^^

DLM 的全名是 Delayable Loaded Module，是 ITE 開發的一項特殊軟體技術(目前申請專利中)，可以將開機階段(第一階段)需載入的程式與在系統進到主頁面後(第二階段)才需要使用的模組(例如: Wifi、bluetooth)分階段載入，藉以縮小開機階段(即第一階段)所需載入的 Image 的大小。而第二階段才需要使用的模組被稱為 DLM。

要開啟此功能必須開啟 qconf 設定與建置工具裡的 :menuselection:`System --> Internal Settings --> Delayable loaded module Enable` 選項，DLM 放在 Rom 裡面的位置可以在qconf 設定與建置工具裡的 :menuselection:`System --> Internal Settings --> Delayable loaded module Enable --> DLM package position` 選項中設定。

  .. image:: /_static/qconf_dlm_enable.png

開啟 DLM 功能時，為了確保 Rom 有足夠的空間可以放置 DLM，有可能需要關閉 qconf 設定與建置工具裡的 :menuselection:`Upgrade --> Backup package for restore` 選項。

  .. image:: /_static/qconf_backup_package_for_restore_disable.png

.. raw:: latex

  \newpage

Bootloader DLM
^^^^^^^^^^^^^^^^

Bootloader 也可以採用 DLM 技術，可以在 Bootloader 執行到特定狀況後再載入需要使用的模組(例如: Wifi、bluetooth)，藉以縮小第一階段所需載入的 Bootloader 的大小。

要開啟此功能必須開啟 qconf 設定與建置工具裡的 :menuselection:`System --> Internal Settings --> Bootloader Delayable loaded module Enable` 選項，放在 Rom 裡面的位置可以在 qconf 設定與建置工具裡的 :menuselection:`System --> Internal Settings --> Bootloader Delayable loaded module Enable --> Bootloader DLM package position` 選項中設定。

  .. image:: /_static/qconf_bootloader_dlm_enable.png


.. raw:: latex

  \newpage


UPGRADE_MARK_POS
^^^^^^^^^^^^^^^^^^^^^^^

在 ITE 提供的韌體更新程式碼中，在 Upgrade PKG 時，會對 NOR flash 的 UPGRADE_MARK_POS 位址處寫入特殊的編號來記錄這次更新是否失敗，若更新失敗在下次開機後又會嘗試進行韌體更新。可以透過設定 qconf 設定與建置工具裡的 :menuselection:`System --> Internal Settings --> Upgrade mark position` 來改變該寫入位址，必須注意 UPGRADE_MARK_POS 的值不能覆蓋到 bootloader 和 image 區間，必須定義在下圖的斜線區塊內。

  .. image:: /_static/diagram_upgrade_mark_pos.png


  .. image:: /_static/qconf_upgrade_mark_pos.png


PKG 更新方式
----------------------------------------------------------------

ITE SDK 原本的設計是在 bootloader 內偵測特定 storage 裝置中是否存在 PKG 檔以進行韌體更新。為了加速開機速度，在開啟 qconf 設定與建置工具裡的 :menuselection:`Upgrade --> Force skip Upgrade in boot time` 選項後，此更新模式將被禁用。系統將改採用以下描述的方式來進行韌體更新。

Normal 更新
^^^^^^^^^^^^^^^^^^^

原本在 bootloader 去偵測 storage 是否有 PKG，若有便進行更新的方式，改為在進入主程式的主頁面後，才偵測特定 key 是否長按來決定是否進行韌體更新。

- 長按 Reboot key --> 偵測 storage 是否有 PKG --> 進行更新


Recovery 更新
^^^^^^^^^^^^^^^^^^^

當 PKG 更新失敗時，我們提供兩種方式可以在 bootloader 重新燒錄 PKG，
此功能目前只有在使用者將特定的 GPIO 接上後才能作用。當 bootloader 偵測到前一次更新失敗後，會依照使用者設定的 GPIO 來進行相對應的更新方式，分別有以下兩種:

- 自動偵測 storage 是否有 PKG --> 進行更新

  使用者可以在 qconf 設定與建置工具裡的 :menuselection:`Display Control Board --> Storage Upgrade Pin` 設定要開啟此功能需要接上的 GPIO。

  .. image:: /_static/qconf_storage_upgrade_pin.png

.. raw:: latex

  \newpage


- 自動連上wifi --> 從 FTP server or HTTP server download PKG --> 進行更新

  .. raw:: latex

    \begin{samepage}

  使用者可以在 qconf 設定與建置工具裡的 :menuselection:`Display Control Board --> Wifi Upgrade Pin` 設定要開啟此功能需要接上的 GPIO。

  .. raw:: latex

    \nopagebreak

  .. image:: /_static/qconf_wifi_upgrade_pin.png

  .. raw:: latex

    \end{samepage}

  .. raw:: latex

    \begin{samepage}

  無線網路的 SSID 和密碼可以在 qconf 設定與建置工具裡的 :menuselection:`Display Control Board --> STA SSID` 和 :menuselection:`Display Control Board --> STA Password` 進行設定。

  .. raw:: latex

    \nopagebreak

  .. image:: /_static/qconf_sta_ssid.png

  .. raw:: latex

    \end{samepage}

  .. raw:: latex

    \begin{samepage}

  網路更新路徑可以在 qconf 設定與建置工具裡的 :menuselection:`Display Control Board --> Upgrade URL` 進行設定。

  .. raw:: latex

    \nopagebreak

  .. image:: /_static/qconf_upgrade_url.png

  .. raw:: latex

    \end{samepage}

  .. raw:: latex

    \begin{samepage}

  需注意，如果要使用 FTP server download PKG，必須勾選 qconf 設定與建置工具裡的 :menuselection:`Network --> Enable FTP protocol` 選項。

  .. raw:: latex

    \nopagebreak

  .. image:: /_static/qconf_enable_ftp_protocol.png

  .. raw:: latex

    \end{samepage}

