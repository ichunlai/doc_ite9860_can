.. _can_bus_setting:

Canbus 設定說明
=====================================

本章說明在 automotive_canbus 專案與 Canbus 相關的設定。

Kconfig 設定
---------------------

.. image:: /_static/qconf_canbus_enable.png

- :menuselection:`Peripheral --> CANBUS Enable`

  開啟 Canbus driver 支援。

- :menuselection:`Peripheral --> CANBUS XCVR Enable`

  開啟 Canbus Transceiver driver 支援。

  目前支援Transceiver型號:

  - MICROCHIP ATA6570
  - NXP TJA1145A

Canbus Task
-----------------------

- 負責處理 canbus 訊息主要執行緒
- Code Path : :file:`<sdk_root>/sdk/share/can_hal/can_handle.c`
- MainTask 呼叫 ``CanInit()`` 啟動執行緒

  .. image:: /_static/diagram_can_init.png

- 呼叫 ``canExit()`` 則可以結束執行緒
- ``canSleep()`` 進入 sleep mode (全斷電模式)



Canbus Transceiver (ata6563)
------------------------------------

- Communication speed up to 5Mbps
- Wake-up via RXD Output pin
- Remote Wake- up pattern according to ISO 11898-2:2016
- Operating Modes

  .. image:: /_static/diagram_can_ata6563.png

.. raw:: latex

  \newpage


Canbus Transceiver (ata6570)
------------------------------------

- 4Mbit/s SPI interface, communication speed up to 5Mbps
- Power Down via INH Output pin
- Remote Wake- up pattern according to ISO 11898-2:2016
- Remote Wake- up frame according to ISO 11898-2:2016
- Device Operation Modes

  .. image:: /_static/diagram_can_ata6570.png

.. raw:: latex

  \newpage

API description
------------------------------------

ithATA6570Init
^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithATA6570Init(uint8_t port, ATA6570_CAN_BAUDRATE datarate)

**Parameters**

``uint8_t port``

  SPI interface 0 or 1.

``ATA6570_CAN_BAUDRATE datarate``

  Config data rate.

**Description**

Initialize canbus transceiver controller(ATA6570).


ithATA6570SetOPMode
^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithATA6570SetOPMode(uint8_t port, ATA6570_CAN_OPMODE mode)

**Parameters**

``uint8_t port``

  SPI interface 0 or 1.

``ATA6570_CAN_OPMODE mode``

  Set mdoe id.

**Description**

Set Devices operation mode.



ithATA6570GetOPMode
^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: ATA6570_CAN_OPMODE ithATA6570GetOPMode(uint8_t port)

**Parameters**

``uint8_t port``

  SPI interface 0 or 1.

**Return**

  mode id (ATA6570_CAN_OPMODE).

**Description**

Get Devices operation mode.


ithATA6570GetModeStatus
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: uint8_t ithATA6570GetOPMode(uint8_t port)

**Parameters**

``uint8_t port``

  SPI interface 0 or 1.

**Return**

  uint8_t

  - bit5: Normal mode transition status
  - bit6: Overtemperature prewarning status
  - bit7: Sleep mode transition status


**Description**

Get device operation mode transtion related information.

ithATA6570GetCOMode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: ATA6570_CAN_COMODE ithATA6570GetCOMode(uint8_t port)

**Parameters**

``uint8_t port``

  SPI interface 0 or 1.

**Return**

  ATA6570_CAN_COMODE

  - 0x00 TRX stanby mode
  - 0x01 TRX Normal mode (VCC undervoltage detection active)
  - 0x10 TRX Normal mode (VCC undervoltage detection inactive)
  - 0x11 TRX Silent mode


**Description**

Get CAN TRX operation mode.
