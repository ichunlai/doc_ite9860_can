LittleFS 使用指南
==========================

為了確保斷電後儲存的資料能夠復原，ITE SDK 支援了 littleFS 檔案系統 (https://github.com/littlefs-project/littlefs)。不過由於 littleFS 的日誌系統的實作機制會導致儲存空間上大量的耗費，因此 ITE SDK 支援雙檔案系統。用戶可將不太更動的資料採用 FAT 檔案系統儲存，而需要復原保護的資料才放到使用 littleFS 檔案系統的 partition。

雙檔案系統的支援目前有限制，僅在開啟 qconf 設定與建置工具裡的 :menuselection:`Upgrade --> LFS partitions with a FAT partition #0` 的選項後才支援雙檔案系統，並且只有第一個 partition 為 FAT 檔案格式，其餘 partition 皆是 LFS 檔案格式。

KConfig 設定
-------------------------

- :menuselection:`File System --> FAT File System`
- :menuselection:`File System --> Littlefs File System`

  這兩個選項分別啟用 FAT 和 LFS 檔案系統。若同時啟用了 FAT 和 LFS 檔案系統，除非啟用 :menuselection:`Upgrade --> LFS partitions with a FAT partition #0`，否則所有內部儲存裝置的所有 partition 將被格式化為 LFS 檔案系統。至於外部儲存裝置(譬如 SD 或 USB disk)則根據磁碟上的檔案系統自動偵測。

- :menuselection:`Display Control Board --> Checking files' CRC on booting time`

  若 main device 採取 LFS, 須將 CHECK_FILES_CRC_ON_BOOTING 取消，直接透過 LFS 保護機制來實作雙備份。

- :menuselection:`File System --> NOR reserved size`
- :menuselection:`File System --> NOR disk partition #`
- :menuselection:`File System --> The partition # size of NOR disk`

  若 main device 採取 LFS, NOR reserved size 須人工向前預留一個 64 KB Block (假定 NOR_RESERVED_SIZE 設為 0x250000, 實際上 LFS 的設計會使用到 0x240000, 須留意)

  若 main device 採取 LFS, 每新建一個目錄，將用掉 64 KB(one Block) * 2, 最小 NOR partition size 就是 128 KB (僅根目錄), 目錄的最小保險空間可以這樣規劃，預期的資料夾數量 * 64 KB * 2, 而某個檔案資料的最小保險空間，為預期資料量(以 Block 為單位取 ceil) 的兩倍，加總即為 NOR partition size。

- :menuselection:`Upgrade --> LFS partitions with a FAT partition #0`

  在 FAT 與 LFS 同時開啟下，若勾選 UPGRADE_LFS_FAT_ON_PARTITION0，main device 便開啟同時含 FAT 和 LFS file system 的功能，目前僅支援 partition_0 建為 FAT，其餘 partition 建為 LFS，不得更改。另外，若未來的更新修改了 file system type 之定義，就須要重燒 bootloader 來更新 PKG，以及重新配置 NOR。

POSIX API Support
-------------------------

1. no timestamps implementation in the official release

2. 支援 POSIX 的 file system API, 使用方法可以參考 3. Test Project

3.	Test Project

  1. test project path

    :file:`<sdk_root>\project\standard_project_lfs_test`

  2. 測試方式

    使用 9860 standard EVB, 透過 PKG 或 ROM 升級，升級時請勿斷電。

測試流程
---------------

reserved zone: 11M byte

A partition: 7M byte

B partition: 7M byte

C partition: 7M byte

11 test binary: 1, 50, 100, 500, 1K, 50K, 100K, 500K, 1M, 2M, 3M byte

6 test files: 2 files in A partition, 2 files in B partition, 2 files in C partition

11 test binaries are reserved in data section of project, test files are initialized 1, 3M, 50, 2M, 100, 1M test binary respectively

開機後，循序檢查 6 test files 與 data section 是否吻合，不吻合會報錯，程式中止。接著進入迴圈，循序對 6 test files 做 data section 內容檢查，及覆寫的動作，若 test file 當下為 50 byte binary, 比對正確後，將會覆蓋為下一個 binary 100 byte(data section), 若 test file 當下為 3M byte, 下一次會填入 1 byte, 以此類推。所有 API 若出現異常，便會報錯並中止程式，若無報錯，30 秒後機器會自動重啟，模擬重新上電，若客戶要自行斷電測試，也是可以。

注意事項
---------------

- 壓力測了兩天半是正常運作的，另外手動斷電一百次，也是正常。
- NOR 存在寫入壽命的限制，每個 Block 10 萬次，若持續寫同一個 Block, 最快 10 小時會壞。
- 由於 NOR erase 時間不一定，端看環境。客戶可以自行修改重啟時間。 static int timeout = 30;
